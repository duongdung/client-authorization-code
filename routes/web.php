<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/redirect', function () {
//     $query = http_build_query([
//         'client_id' => '2',
//         'redirect_uri' => 'https://bob.dev/home',
//         'response_type' => 'token',
//         'scope' => ''
//     ]);
//         return redirect('https://codegym.sis.dev/oauth/authorize?'.$query);
// })->name('login.passport');

Route::get('/redirect', function () {
    $query = http_build_query([
        'client_id' => '2',
        'redirect_uri' => 'http://codegym.bob.dev/callback',
        'response_type' => 'code',
        'scope' => ''
    ]);

    return redirect('http://codegym.sis.dev/oauth/authorize?'.$query);
})->name('login.passport');

Route::get('/callback', function (Request $request) {
    $http = new GuzzleHttp\Client;

    $response = $http->post('http://codegym.sis.dev/oauth/token', [
        'form_params' => [
            'grant_type' => 'authorization_code',
            'client_id' => '2',
            'client_secret' => 'Q1f7HEnuZ7iHATAS8MsAQKOrYaaRvmaG0osLmq7m',
            'redirect_uri' => 'http://codegym.bob.dev/callback',
            'code' => $request->code
        ],
    ]);

    // access token
    $body = json_decode((string) $response->getBody(), true);

    Session::push('accsess_token', $body['access_token']);
    // info user
    $response = $http->get('http://codegym.sis.dev/api/user', [
        'headers' => [
            'Authorization' => 'Bearer ' . $body['access_token'],
        ],
    ]);

    $user = json_decode((string) $response->getBody(), true);

    return redirect()->route('home');
    // return $user;
});


Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/home', 'HomeController@index')->name('home');
